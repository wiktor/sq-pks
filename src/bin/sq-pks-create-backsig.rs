use std::path::PathBuf;
use std::time::SystemTime;

use openpgp::packet::signature;
use openpgp::parse::Parse;
use openpgp::serialize::Marshal;
use openpgp::types::{HashAlgorithm, SignatureType};
use openpgp::{Cert, Packet, PacketPile};
use sequoia_openpgp as openpgp;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "sq-pks-create-backsig")]
struct Opt {
    #[structopt(long, default_value = "http://localhost:3000/")]
    private_key_store: String,

    #[structopt(long, short, parse(from_os_str))]
    subkey: PathBuf,

    #[structopt(long, short, parse(from_os_str))]
    cert: PathBuf,

    #[structopt(long, parse(from_os_str))]
    password_dir: PathBuf,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut opt = Opt::from_args();

    let pp = PacketPile::from_file(opt.subkey)?;
    let key = if let Some(Packet::PublicSubkey(ref signing_key)) = pp.path_ref(&[0]) {
        signing_key.role_as_unspecified()
    } else {
        panic!("No key found.");
    };

    opt.password_dir.push(key.fingerprint().to_hex());
    let p = std::fs::read_to_string(opt.password_dir)?.into();

    let mut subkey_signer =
        sequoia_net::pks::unlock_signer(opt.private_key_store, key.clone(), &p)?;

    let cert = Cert::from_file(opt.cert)?;

    let backsig = signature::SignatureBuilder::new(SignatureType::PrimaryKeyBinding)
        .set_signature_creation_time(SystemTime::now())?
        .set_hash_algo(HashAlgorithm::SHA256)
        .sign_primary_key_binding(
            &mut subkey_signer,
            &cert.primary_key(),
            key.role_as_subordinate(),
        )?;

    let mut stdout = std::io::stdout();
    backsig.serialize(&mut stdout)?;

    Ok(())
}
