use std::io;
use std::path::PathBuf;

use openpgp::cert::prelude::*;
use openpgp::crypto::{Decryptor, SessionKey};
use openpgp::packet::PKESK;
use openpgp::parse::{stream::*, Parse};
use openpgp::policy::StandardPolicy as P;
use openpgp::types::SymmetricAlgorithm;
use openpgp::Fingerprint;
use sequoia_net::pks;
use sequoia_openpgp as openpgp;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "sq-pks-decrypt")]
struct Opt {
    #[structopt(long, default_value = "http://localhost:3000/")]
    private_key_store: String,

    #[structopt(long, short, parse(from_os_str))]
    cert: PathBuf,

    #[structopt(long, parse(from_os_str))]
    password_dir: PathBuf,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let policy = &P::new();

    let opt = Opt::from_args();

    let cert = Cert::from_file(opt.cert)?;

    let mut decryptors = vec![];

    for key in cert
        .keys()
        .with_policy(policy, None)
        .alive()
        .revoked(false)
        .for_transport_encryption()
        .for_storage_encryption()
        .supported()
        .map(|ka| ka.key())
    {
        let mut password_file = opt.password_dir.clone();
        password_file.push(key.fingerprint().to_hex());
        let password: openpgp::crypto::Password = std::fs::read_to_string(&password_file)
            .map_err(|e| format!("Error {:?} in {}", e, password_file.display()))?
            .into();

        match pks::unlock_decryptor(&opt.private_key_store, key.clone(), &password) {
            Ok(decryptor) => {
                decryptors.push(decryptor);
            }
            Err(error) => eprintln!("Could not unlock decryptor: {:?}", error),
        }
    }
    let helper = Helper {
        cert: &cert,
        decryptors,
    };

    let stdin = std::io::stdin();
    let mut decryptor = DecryptorBuilder::from_reader(stdin)?.with_policy(policy, None, helper)?;

    let mut sink = std::io::stdout();
    io::copy(&mut decryptor, &mut sink)?;

    Ok(())
}

struct Helper<'a> {
    cert: &'a openpgp::Cert,
    decryptors: Vec<Box<dyn Decryptor + 'a + Send + Sync>>,
}

impl<'a> VerificationHelper for Helper<'a> {
    fn get_certs(&mut self, _ids: &[openpgp::KeyHandle]) -> openpgp::Result<Vec<openpgp::Cert>> {
        Ok(vec![self.cert.clone()])
    }

    fn check(&mut self, _structure: MessageStructure) -> openpgp::Result<()> {
        Ok(())
    }
}

impl<'a> DecryptionHelper for Helper<'a> {
    fn decrypt<D>(
        &mut self,
        pkesks: &[openpgp::packet::PKESK],
        _skesks: &[openpgp::packet::SKESK],
        sym_algo: Option<SymmetricAlgorithm>,
        mut decrypt: D,
    ) -> openpgp::Result<Option<openpgp::Fingerprint>>
    where
        D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
    {
        for pkesk in pkesks {
            for decryptor in &mut self.decryptors {
                if let Some(fp) =
                    Helper::try_decrypt(pkesk, sym_algo, decryptor.as_mut(), &mut decrypt)
                {
                    return Ok(fp);
                }
            }
        }
        Err(openpgp::Error::InvalidKey("No key to decrypt message".into()).into())
    }
}

impl<'a> Helper<'a> {
    fn try_decrypt<D>(
        pkesk: &PKESK,
        sym_algo: Option<SymmetricAlgorithm>,
        keypair: &'a mut dyn Decryptor,
        decrypt: &mut D,
    ) -> Option<Option<Fingerprint>>
    where
        D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
    {
        pkesk
            .decrypt(&mut *keypair, sym_algo)
            .and_then(
                |(algo, sk)| {
                    if decrypt(algo, &sk) {
                        Some(None)
                    } else {
                        None
                    }
                },
            )
    }
}
