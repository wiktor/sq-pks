use std::path::PathBuf;

use openpgp::parse::Parse;
use openpgp::Cert;
use openpgp::{
    serialize::stream::{Armorer, Message, Signer},
    types::HashAlgorithm,
};
use sequoia_openpgp as openpgp;

use openpgp::policy::StandardPolicy;
use sequoia_net::pks;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "sq-pks-sign-detached")]
struct Opt {
    #[structopt(long, default_value = "http://localhost:3000/")]
    private_key_store: String,

    #[structopt(long, short, parse(from_os_str))]
    cert: PathBuf,

    #[structopt(long, parse(from_os_str))]
    password_dir: PathBuf,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let cert = Cert::from_file(opt.cert)?;

    let message = Message::new(std::io::stdout());

    let message = Armorer::new(message).build()?;

    let mut signers = vec![];

    let p = StandardPolicy::new();

    for key in cert
        .keys()
        .with_policy(&p, None)
        .alive()
        .revoked(false)
        .for_signing()
        .supported()
        .map(|ka| ka.key())
    {
        let mut password_file = opt.password_dir.clone();
        password_file.push(key.fingerprint().to_hex());
        let password = std::fs::read_to_string(password_file)?.into();

        match pks::unlock_signer(&opt.private_key_store, key.clone(), &password) {
            Ok(signer) => {
                signers.push(signer);
            }
            Err(error) => eprintln!("Could not unlock signer: {:?}", error),
        }
    }

    // Now, create a signer that emits the signature(s).
    let mut signer = Signer::new(message, signers.pop().unwrap());

    for s in signers {
        signer = signer.add_signer(s);
    }

    signer = signer.hash_algo(HashAlgorithm::SHA256)?;
    let mut signer = signer.detached().build()?;

    // Then, create a literal writer to wrap the data in a literal
    // message packet.
    //let mut literal = LiteralWriter::new(signer).build()?;

    // Copy all the data.
    std::io::copy(&mut std::io::stdin(), &mut signer)?;

    // Finally, teardown the stack to ensure all the data is written.
    signer.finalize()?;
    Ok(())
}
