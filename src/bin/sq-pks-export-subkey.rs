use std::str::FromStr;

use openpgp::crypto::mpi;
use openpgp::packet::key::{Key4, PublicParts, SubordinateRole};
use openpgp::packet::{Key, Packet};
use openpgp::serialize::Serialize;
use sequoia_openpgp as openpgp;

use structopt::StructOpt;

#[derive(Debug)]
enum KeyUsage {
    Signing,
    Encryption,
}

impl FromStr for KeyUsage {
    type Err = openpgp::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "signing" => Ok(Self::Signing),
            "encryption" => Ok(Self::Encryption),
            _ => Err(openpgp::Error::InvalidArgument(
                "Not a valid key usage".into(),
            )),
        }
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "sq-pks-export-subkey")]
struct Opt {
    #[structopt(long = "for")]
    #[allow(dead_code)]
    key_usage: KeyUsage,

    #[structopt(long, default_value = "2021-01-01T00:31:37Z")]
    creation_time: humantime::Timestamp,

    #[structopt(name = "KEY")]
    key: String,
}

#[derive(Debug)]
struct Failure(String);

impl std::fmt::Display for Failure {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl std::error::Error for Failure {}

impl From<String> for Failure {
    fn from(string: String) -> Self {
        Self(string)
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let client = hyper::Client::new();
    let response = client.get(opt.key.try_into()?).await?;
    let success = response.status().is_success();

    let content_type: String = if let Some(header) = response.headers().get("Content-Type") {
        String::from_utf8_lossy(header.as_bytes()).to_string()
    } else {
        "".into()
    };

    let body = hyper::body::to_bytes(response).await?;
    if !success {
        return Err(String::from_utf8_lossy(&body).into());
    }

    let key4: Key<PublicParts, SubordinateRole> = match (content_type.as_ref(), opt.key_usage) {
        ("application/vnd.pks.public.rsa.modulus", _) => {
            let exponent: &[u8] = &[1, 0, 1];
            let modulus: &[u8] = &body;

            Key4::import_public_rsa(exponent, modulus, *opt.creation_time)?.into()
        }
        ("application/vnd.pks.public.p256.compressed", KeyUsage::Encryption) => Key4::new(
            *opt.creation_time,
            openpgp::types::PublicKeyAlgorithm::ECDH,
            mpi::PublicKey::ECDH {
                curve: openpgp::types::Curve::NistP256,
                q: Vec::from(body.as_ref()).into(),
                hash: openpgp::types::HashAlgorithm::SHA256,
                sym: openpgp::types::SymmetricAlgorithm::AES256,
            },
        )?
        .into(),
        ("application/vnd.pks.public.p256.compressed", KeyUsage::Signing) => Key4::new(
            *opt.creation_time,
            openpgp::types::PublicKeyAlgorithm::ECDSA,
            mpi::PublicKey::ECDSA {
                curve: openpgp::types::Curve::NistP256,
                q: Vec::from(body.as_ref()).into(),
            },
        )?
        .into(),
        _ => panic!("Unsupported key type."),
    };

    eprintln!("{}", key4.fingerprint());

    let packet: Packet = key4.into();
    let mut stdout = std::io::stdout();
    packet.serialize(&mut stdout)?;

    Ok(())
}
